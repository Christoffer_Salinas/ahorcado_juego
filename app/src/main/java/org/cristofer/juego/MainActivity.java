package org.cristofer.juego;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private String[] palabra;
    private Random random;
    private String palatraant;
    private TextView[] charViews;
    private LinearLayout wordLayout;
    private LetterAdapter adapter;
    private GridView gridView;
    private int numCorr;
    private int numChars;
    private ImageView[]parts;
    private int sizeParts=5;
    private int currParts;
    private int espacios=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        palabra=getResources().getStringArray(R.array.palabra);
        wordLayout=findViewById(R.id.words);
        gridView=findViewById(R.id.letters);
        random=new Random();
        parts=new ImageView[sizeParts];
        parts[0]=findViewById(R.id.cabeza);
        parts[1]=findViewById(R.id.cuerpo);
        parts[2]=findViewById(R.id.brazos);
        parts[3]=findViewById(R.id.pies);
        parts[4]=findViewById(R.id.muerto);

        playGame();
    }

    private void playGame(){
        String nuevaPalabra=palabra[random.nextInt(palabra.length)];
        currParts=0;
        numCorr=0;
        while (nuevaPalabra.equals(palatraant))nuevaPalabra=palabra[random.nextInt(palabra.length)];
        palatraant=nuevaPalabra;

        charViews= new TextView[palatraant.length()];
         wordLayout.removeAllViews();
        for(int i=0; i<palatraant.length();i++){
            if(palatraant.charAt(i)!=32){
                charViews[i]=new TextView(this);
                charViews[i].setText(""+palatraant.charAt(i));
                charViews[i].setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
                charViews[i].setGravity(Gravity.CENTER);
                charViews[i].setTextColor(Color.WHITE);
                charViews[i].setBackgroundResource(R.drawable.letter_bg);
                wordLayout.addView(charViews[i]);
            }else{
                espacios++;
                charViews[i]=new TextView(this);
                charViews[i].setText("  ");
                wordLayout.addView(charViews[i]);
                numCorr++;
            }
        }

        adapter= new LetterAdapter(this);
        gridView.setAdapter(adapter);


        numChars=palatraant.length();

        for (int i=0;i<sizeParts;i++){
            parts[i].setVisibility(View.INVISIBLE);
        }
    }

    public void letterPressed(View view){
        String letter=((TextView)view).getText().toString();
        char letterChar=letter.charAt(0);

        view.setEnabled(false);
        boolean correct=false;
        for (int i=0;i<palatraant.length();i++){
            if(palatraant.charAt(i)==letterChar){
                correct=true;
                numCorr++;
                charViews[i].setTextColor(Color.BLACK);
            }
        }
        if(correct){
            if(numCorr==numChars){
                disableButtons();
                AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setTitle("Ganaste");
                builder.setMessage("Felicidades \n\n La respuesta es\n\n"+palatraant);
                builder.setPositiveButton("Jugar de nuevo", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        playGame();
                    }
                });

                builder.setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.finish();
                    }
                });
                builder.show();
            }
        }else if (currParts<sizeParts){
            parts[currParts].setVisibility(View.VISIBLE);
            currParts++;
        }else{
            disableButtons();
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setTitle("Perdiste");
            builder.setMessage("La respuesta es\n\n"+palatraant);
            builder.setNegativeButton("Jugar de nuevo", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    playGame();
                }
            });

            builder.setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainActivity.this.finish();
                }
            });
            builder.show();
        }
    }

    public void disableButtons(){
        for(int i=0;i<gridView.getChildCount();i++){
            gridView.getChildAt(i).setEnabled(false);
        }
    }
}